import { generateRequestId, createJsonRpcRequest } from ".";

export const KnimeUtils = {
  generateRequestId,
  createJsonRpcRequest,
};
