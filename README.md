# KNIME® UI Extension Service - ⚠️ Deprecated Repository

> **This repository is now deprecated**  
> Please note that this repository is no longer maintained and will not receive updates.

🚨 **New Location**: This project has been moved to the webapps-common monorepo at [`/packages/ui-extension-service`](https://bitbucket.org/KNIME/webapps-common/src/master/packages/ui-extension-service/).

For future development, contributions, and updates, please refer to the new location. This repository will remain accessible for historical purposes only, but we encourage you to transition to the monorepo as soon as possible.

This repository was maintained by the [KNIME UI Extensions Development Team](mailto:team-ui-extensions@knime.com).
